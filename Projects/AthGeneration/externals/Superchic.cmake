#
# File specifying the location of Superchic 3.0 to use.
#

set( SUPERCHIC_LCGVERSION 3.06 )
set( SUPERCHIC_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/superchic/${SUPERCHIC_LCGVERSION}/${LCG_PLATFORM} )
